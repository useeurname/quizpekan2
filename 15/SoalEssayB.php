<?php
function perolehan_medali($array)
{
    if ($array == []) {
        return "no data";
    }
    $temp = [];
    for ($i = 0; $i < count($array); $i++) {
        $temp[$i] = $array[$i][0];
    }

    $dataNegara = array_unique($temp);

    $answer = [];
    for ($i = 0; $i < count($dataNegara); $i++) {
        $answer[$i]["negara"] = $dataNegara[$i];
        $answer[$i]["emas"] = 0;
        $answer[$i]["perak"] = 0;
        $answer[$i]["perunggu"] = 0;
    }

    for ($i = 0; $i < count($array); $i++) {
        for ($j = 0; $j < count($answer); $j++) {
            if ($array[$i][0] == $answer[$j]["negara"]) {
                switch ($array[$i][1]) {
                    case "emas":
                        $answer[$j]["emas"]++;
                        break;
                    case "perak":
                        $answer[$j]["perak"]++;
                        break;
                    case "perunggu":
                        $answer[$j]["perunggu"]++;
                        break;
                    default:
                        continue;
                }
            }
        }
    }

    return $answer;
}
